# frozen_string_literal: true

require 'httparty'

class AnthropicApi
  ANTHROPIC_API_URL = 'https://api.anthropic.com/v1/messages'
  ANTHROPIC_MODEL = 'claude-3-5-sonnet-20241022'

  def self.execute(system_prompt, prompt)
    anthropic_api_key = ENV.fetch('ANTHROPIC_API_KEY')
    response = HTTParty.post(
      ANTHROPIC_API_URL,
      body: {
        model: ANTHROPIC_MODEL,
        system: [
          {
            type: 'text',
            text: system_prompt,
            cache_control: { type: 'ephemeral' }
          }
        ],
        messages: [
          { role: 'user', content: prompt }
        ],
        max_tokens: 1024
      }.to_json,
      headers: {
        'Content-Type' => 'application/json',
        'x-api-key' => anthropic_api_key
      }
    )
    response.parsed_response['content'][0]['text']
  end
end
