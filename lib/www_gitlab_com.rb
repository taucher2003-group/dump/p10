# frozen_string_literal: true

require 'json'
require 'yaml'
require 'httparty'

require_relative '../triage/triage'

class WwwGitLabCom
  WWW_GITLAB_COM_SITE = 'https://about.gitlab.com'
  WWW_GITLAB_COM_SECTIONS_JSON = "#{WWW_GITLAB_COM_SITE}/sections.json".freeze
  WWW_GITLAB_COM_STAGES_JSON = "#{WWW_GITLAB_COM_SITE}/stages.json".freeze
  WWW_GITLAB_COM_GROUPS_JSON = "#{WWW_GITLAB_COM_SITE}/groups.json".freeze
  WWW_GITLAB_COM_CATEGORIES_JSON = "#{WWW_GITLAB_COM_SITE}/categories.json".freeze
  WWW_GITLAB_COM_TEAM_YML = "#{WWW_GITLAB_COM_SITE}/company/team/team.yml".freeze
  ROULETTE_JSON = 'https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json'
  RELEASES = 'https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/releases.yml'
  DISTRIBUTION_PROJECTS_YML = 'https://gitlab.com/gitlab-org/distribution/monitoring/-/raw/master/lib/data_sources/projects.yaml'
  DATA_CACHE_DEFAULT_EXPIRATION = 60 * 30 # 30 minutes
  PROJECTS_WITH_EXTERNAL_REVIEW_PROCESSES = [
    # https://gitlab.com/gitlab-org/security-products/gemnasium-db
    # The Vulnerability Research team has a rotating
    # reviewer for this project who reviews all MRs.
    12006272,
    # Exclude select Quality projects that have their own workflow
    # https://gitlab.com/gitlab-org/gitlab-environment-toolkit
    # https://gitlab.com/gitlab-org/quality/performance
    14292404,
    11511606
  ].freeze

  def self.sections
    Triage.cache.get_or_set(:sections, expires_in: DATA_CACHE_DEFAULT_EXPIRATION) do
      fetch_json(WWW_GITLAB_COM_SECTIONS_JSON)
    end
  end

  def self.stages
    Triage.cache.get_or_set(:stages, expires_in: DATA_CACHE_DEFAULT_EXPIRATION) do
      fetch_json(WWW_GITLAB_COM_STAGES_JSON)
    end
  end

  def self.groups
    Triage.cache.get_or_set(:groups, expires_in: DATA_CACHE_DEFAULT_EXPIRATION) do
      fetch_json(WWW_GITLAB_COM_GROUPS_JSON)
    end
  end

  def self.categories
    Triage.cache.get_or_set(:categories, expires_in: DATA_CACHE_DEFAULT_EXPIRATION) do
      fetch_json(WWW_GITLAB_COM_CATEGORIES_JSON)
    end
  end

  def self.team_from_www
    Triage.cache.get_or_set(:team_from_www, expires_in: DATA_CACHE_DEFAULT_EXPIRATION) do
      fetch_yml(WWW_GITLAB_COM_TEAM_YML).each_with_object({}) do |item, memo|
        memo[item['gitlab']] = item
      end
    end
  end

  def self.roulette
    Triage.cache.get_or_set(:roulette, expires_in: DATA_CACHE_DEFAULT_EXPIRATION) do
      fetch_json(ROULETTE_JSON)
    end
  end

  def self.releases
    Triage.cache.get_or_set(:releases, expires_in: DATA_CACHE_DEFAULT_EXPIRATION) do
      fetch_yml(RELEASES)
    end
  end

  def self.distribution_projects
    Triage.cache.get_or_set(:distribution_projects, DATA_CACHE_DEFAULT_EXPIRATION) do
      fetch_yml(DISTRIBUTION_PROJECTS_YML)
    end
  end

  def self.projects_with_external_review_process
    Triage.cache.get_or_set(:projects_with_external_review_process) do
      PROJECTS_WITH_EXTERNAL_REVIEW_PROCESSES
    end
  end

  def self.fetch_json(json_url)
    json = with_retries { HTTParty.get(json_url, format: :plain) }
    JSON.parse(json)
  end
  private_class_method :fetch_json

  def self.fetch_yml(yaml_url)
    YAML.load(HTTParty.get(yaml_url), permitted_classes: [Symbol, Date])
  end
  private_class_method :fetch_yml

  def self.with_retries(attempts: 3)
    yield
  rescue Errno::ECONNRESET, OpenSSL::SSL::SSLError, Net::OpenTimeout
    retry if (attempts -= 1).positive?
    raise
  end
  private_class_method :with_retries
end
