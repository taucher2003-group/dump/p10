# frozen_string_literal: true

require_relative '../triage/job'
require_relative '../resources/merge_request'

module Triage
  class DocsOnlyLabellerJob < Job
    include Reaction

    DOCS_ONLY_REGEX = %r{\Adocs?/}

    private

    def execute(event)
      prepare_executing_with(event)

      if needs_adding?
        add_comment("/label ~#{Labels::DOCS_ONLY_LABEL}", append_source_link: false)
      elsif needs_removing?
        add_comment("/unlabel ~#{Labels::DOCS_ONLY_LABEL}", append_source_link: false)
      end
    end

    def needs_adding?
      !label_names.include?(Labels::DOCS_ONLY_LABEL) &&
        !label_names.include?(Labels::BACKEND_LABEL) &&
        only_docs_changes?
    end

    def needs_removing?
      label_names.include?(Labels::DOCS_ONLY_LABEL) &&
        (label_names.include?(Labels::BACKEND_LABEL) || !only_docs_changes?)
    end

    def only_docs_changes?
      changed_file_list.only_change?(DOCS_ONLY_REGEX)
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def label_names
      @label_names ||= merge_request.labels
    end

    def merge_request
      @merge_request ||= Triage::MergeRequest.build_from_api(event.api_client.merge_request(project_id, iid))
    end

    def project_id
      event.project_id
    end

    def iid
      event.iid
    end
  end
end
