# frozen_string_literal: true

require_relative '../triage/job'

module Triage
  class AddCommentJob < Job
    include Reaction

    private

    def execute(event, comment)
      prepare_executing_with(event)

      add_comment(comment, append_source_link: false)
    end
  end
end
