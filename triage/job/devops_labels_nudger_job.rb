# frozen_string_literal: true

require_relative '../triage/job'
require_relative '../triage/devops_labels_validator'
require_relative '../resources/issue'

module Triage
  class DevopsLabelsNudgerJob < Job
    include Reaction

    LABELS_MISSING_MESSAGE = <<~MARKDOWN.strip
      Please add a [group](https://docs.gitlab.com/ee/development/labels/#group-labels) or
      [category](https://docs.gitlab.com/ee/development/labels/#category-labels) label to identify issue ownership.

      You can refer to the [Features by Group](https://handbook.gitlab.com/handbook/product/categories/features/#features-by-group) handbook page for guidance.

      If you are unsure about the correct group, please do not leave the issue without a group label, and refer to
      [GitLab's shared responsibility functionality guidelines](https://handbook.gitlab.com/handbook/product/categories/#shared-responsibility-functionality)
      for more information on how to triage this kind of issue.
    MARKDOWN

    private

    def execute(event)
      prepare_executing_with(event)

      add_devops_labels_reminder if applicable?
    end

    def applicable?
      event.from_gitlab_org_gitlab? &&
        event.by_team_member? &&
        !validator.labels_set? &&
        unique_comment.no_previous_comment?
    end

    def add_devops_labels_reminder
      add_comment(reminder_text, append_source_link: true)
    end

    def reminder_text
      message_markdown = <<~MARKDOWN.chomp
        :wave: @#{event.event_actor_username}, #{LABELS_MISSING_MESSAGE}
      MARKDOWN

      unique_comment.wrap(message_markdown).strip
    end

    def validator
      @validator ||= DevopsLabelsValidator.new(event)
    end

    def unique_comment
      @unique_comment ||= validator.unique_comment
    end
  end
end
