# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/event'
require_relative '../resources/issue'

module Triage
  class EngineeringAllocationLabelsValidator
    PRODUCER_LABEL_PREFIX = 'Eng-Producer::'
    CONSUMER_LABEL_PREFIX = 'Eng-Consumer::'
    PRIORITY_LABEL_PREFIX = 'priority::'
    SEVERITY_LABEL_PREFIX = 'severity::'
    BUG_LABEL = 'bug'

    PROCESSOR_CLASS_NAME = 'Triage::EngineeringAllocationLabelsReminder'

    attr_reader :event

    def initialize(event)
      @event = event
    end

    def label_nudge_needed?
      unique_comment.no_previous_comment? && !issue_has_required_labels?
    end

    def unique_comment
      @unique_comment ||= Triage::UniqueComment.new(PROCESSOR_CLASS_NAME, event)
    end

    def issue_has_required_labels?
      checks = [
        :has_producer_label?,
        :has_consumer_label?,
        :has_priority_label?,
        :has_severity_label?
      ]

      checks.all? { |check| method(check).call(issue.labels) }
    end

    private

    def has_producer_label?(labels)
      labels.any? { |label| label.start_with?(PRODUCER_LABEL_PREFIX) }
    end

    def has_consumer_label?(labels)
      labels.any? { |label| label.start_with?(CONSUMER_LABEL_PREFIX) }
    end

    def has_priority_label?(labels)
      labels.any? { |label| label.start_with?(PRIORITY_LABEL_PREFIX) }
    end

    def has_severity_label?(labels)
      !labels.include?(BUG_LABEL) || labels.any? { |label| label.start_with?(SEVERITY_LABEL_PREFIX) }
    end

    def issue
      @issue ||= Triage.api_client.issue(event.project_id, event.iid)
    end
  end
end
