# frozen_string_literal: true

require_relative '../triage'

module Triage
  # Allow processors to react to commands.
  #
  # The concern shouldn't be included directly, but instead it can be enabled in a processor with
  #
  #   define_command name: 'command_name'
  #
  #   # Aliases can be defined
  #   define_command name: 'command_name', aliases: %w[alias_command another_alias]
  #
  #   # A regex can be defined to detect command arguments
  #   define_command name: 'command_name', args_regex: /\w+/
  #
  # The `command.valid?(event)` method should be called in the `#applicable?` method of the processor.
  # The `command.args(event)` method can be used to access the arguments passed to the command.
  # The `command.command(event)` method can be used to access the command name or alias that was used in the comment.
  module ReactiveCommand
    COMMAND_PREFIX_REGEXP = /#{Regexp.escape(::Triage::GITLAB_BOT)}[[:blank:]]+/

    def define_command(name:, aliases: [], args_regex: nil)
      define_method(:command) do
        @command ||= ReactiveCommand::Command.new(name: name, aliases: aliases, args_regex: args_regex)
      end
    end

    class Command
      attr_reader :names, :args_regex

      def initialize(name:, aliases: [], args_regex: nil)
        @names = [*name, *aliases]
        @args_regex = args_regex
      end

      def valid?(event)
        event.new_comment.match?(command_with_args_regex)
      end

      def command(event)
        command_match(event)[1]
      end

      def args(event)
        if @args_regex
          matching_command_with_args(event)
            .sub(command_regexp, '')
            .scan(@args_regex)
            .flatten
            .compact
        else
          []
        end
      end

      private

      def command_regexp
        /^#{COMMAND_PREFIX_REGEXP}(#{@names.map { |name| Regexp.escape(name) }.join('|')})/
      end

      def command_with_args_regex
        /^#{command_regexp}[[:blank:]]*.*$/
      end

      def command_match(event)
        event.new_comment.match(command_with_args_regex)
      end

      def matching_command_with_args(event)
        command_match(event)[0]
      end
    end
  end
end
