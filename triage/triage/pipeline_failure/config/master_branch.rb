# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class MasterBranch < Base
        INCIDENT_PROJECT_ID = '40549124' # gitlab-org/quality/engineering-productivity/master-broken-incidents
        INCIDENT_LABELS = [].freeze
        INCIDENT_TEMPLATE = <<~MARKDOWN.freeze
          #{DEFAULT_INCIDENT_SUMMARY_TABLE}

          %<attribution_body>s

          ## How to close this incident

          - Follow the steps in the [Broken `master` handbook guide](https://handbook.gitlab.com/handbook/engineering/workflow/#broken-master) to
            - [escalate](https://handbook.gitlab.com/handbook/engineering/workflow/#broken-master-escalation)
            - [triage](https://handbook.gitlab.com/handbook/engineering/workflow/#triage-broken-master), and
            - [resolve](https://handbook.gitlab.com/handbook/engineering/workflow/#resolution-of-broken-master)
          - Reminder: apply the appropriate `~master-broken:*` label to document root cause before closing the incident.

          **Quick Tips:**
          - you can retry all failing jobs with `@gitlab-bot retry_pipeline %<pipeline_id>s`.
          - a message can be posted in `#backend_maintainers` or `#frontend_maintainers` to get a maintainer take a look at the fix ASAP.
          - add the ~"pipeline::expedited" label, and `master:broken` or `master:foss-broken` label, to speed up the `master`-fixing pipelines.
        MARKDOWN

        SLACK_CHANNEL = 'master-broken'
        APPLICABLE_PROJECT_PATHS = ['gitlab-org/gitlab', 'gitlab-org/gitlab-foss'].freeze

        def self.match?(event)
          event.on_instance?(:com) &&
            APPLICABLE_PROJECT_PATHS.include?(event.project_path_with_namespace) &&
            !event.merge_request_pipeline? &&
            event.ref == 'master' &&
            event.source_job_id.nil? # exclude triggered pipeline
        end

        def incident_project_id
          INCIDENT_PROJECT_ID
        end

        def incident_template
          INCIDENT_TEMPLATE
        end

        def incident_labels
          master_broken_label =
            if event.project_path_with_namespace.end_with?('gitlab-foss')
              'master:foss-broken'
            else
              'master:broken'
            end

          [*INCIDENT_LABELS, master_broken_label]
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end

        def auto_triage?
          true
        end

        def escalate_on_stale?
          true
        end
      end
    end
  end
end
