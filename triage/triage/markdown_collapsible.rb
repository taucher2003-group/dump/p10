# frozen_string_literal: true

module Triage
  class MarkdownCollapsible
    COLLAPSIBLE_COMMENT_REGEX = %r{</summary>\n\n(.*?)\n</details>}m

    attr_reader :title, :content

    def initialize(title, content)
      @title   = title
      @content = content
    end

    def collapsed_content
      <<~SUMMARY
        <details>
        <summary markdown="span">#{title}</summary>

        #{content}
        </details>
      SUMMARY
    end

    def uncollapsed_content
      content[COLLAPSIBLE_COMMENT_REGEX, 1].strip
    end
  end
end
