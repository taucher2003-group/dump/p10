# frozen_string_literal: true

require_relative '../../../lib/constants/labels'
require_relative './../label_implementation_plan'

module QuickWin
  module MessageFormatter
    include Triage::LabelImplementationPlan

    def quick_win_validation_message
      <<~TEXT
        Hey @#{mention_users.join(', @')}, #{message}

        If you believe this issue is still relevant, ensure it meets the [criteria for quick win issues](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows/#criteria-for-quick-win-issues)
        then re-add the label.

        Please direct any questions to `@gitlab-org/developer-relations/contributor-success`.

        /unlabel ~"#{Labels::QUICK_WIN}"
        /label ~"#{Labels::AUTOMATION_QUICK_WIN_REMOVED}"
      TEXT
    end

    private

    def message
      return criteria_message if has_empty_or_no_implementation_plan?(description)

      age_message
    end

    def criteria_message
      "it appears this issue does not meet all of the required criteria for the ~\"#{Labels::QUICK_WIN}\" label, so it has been removed."
    end

    def age_message
      "it has been over a year since the ~\"#{Labels::QUICK_WIN}\" label was added, so it has been removed."
    end

    def mention_users
      added_by_username = label_added_username
      return [author] if author == added_by_username || added_by_username.blank?

      [added_by_username, author]
    end

    def author
      resource.dig(:author, :username)
    end

    def description
      resource[:description]
    end

    def label_added_username
      label_event_finder.label_added_username
    end
  end
end
