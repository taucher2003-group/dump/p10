# frozen_string_literal: true

module Triage
  module DateHelper
    def today_is_weekend?
      day_of_week = Time.now.utc.wday
      day_of_week.zero? || day_of_week == 6
    end
  end
end
