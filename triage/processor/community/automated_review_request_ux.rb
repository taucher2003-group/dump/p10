# frozen_string_literal: true

require 'digest'
require 'slack-messenger'

require_relative 'community_processor'

module Triage
  class AutomatedReviewRequestUx < CommunityProcessor
    SLACK_CHANNEL = '#ux-community-contributions'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hi UX team, a new community contribution (%<mr_title>s) requires a UX review: %<mr_url>s.
    MESSAGE

    react_to 'merge_request.update'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      wider_community_contribution_open_resource? &&
        !event.wip? &&
        ux_label? &&
        unique_comment.no_previous_comment?
    end

    def process
      send_review_request
      post_ux_comment
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    def documentation
      <<~TEXT
        This processor pings the UX team to review community merge requests labeled with ~"#{Labels::UX_LABEL}".
      TEXT
    end

    private

    attr_reader :messenger

    def ux_label?
      event.label_names.include?(Labels::UX_LABEL)
    end

    def send_review_request
      slack_message = format(SLACK_MESSAGE_TEMPLATE, mr_url: event.url, mr_title: event.title)
      messenger.ping(slack_message)
    end

    def post_ux_comment
      add_comment(message.strip, append_source_link: true)
    end

    def message
      comment = <<~MESSAGE
        Thanks for helping us improve the UX of GitLab. Your contribution is appreciated! We have pinged our UX team, so stay tuned for their feedback.
      MESSAGE

      unique_comment.wrap(comment)
    end

    def slack_messenger
      Slack::Messenger.new(ENV.fetch('SLACK_WEBHOOK_URL', nil), slack_options)
    end
  end
end
