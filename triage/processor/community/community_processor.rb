# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'
require_relative '../../triage/unique_comment'

module Triage
  class CommunityProcessor < Processor
    def wider_community_contribution?
      label_names.include?(Labels::COMMUNITY_CONTRIBUTION_LABEL) ||
        label_names.include?(Labels::CONTRACTOR_CONTRIBUTION_LABEL)
    end

    def wider_community_contribution_open_resource?
      event.resource_open? &&
        wider_community_contribution?
    end

    def valid_command?
      command.valid?(event) &&
        (
          event.by_resource_author? ||
            event.by_resource_assignee? ||
            event.by_team_member? ||
            event.by_resource_reviewer? ||
            event.by_community_forks_member?
        )
    end

    def workflow_in_dev_added?
      event.added_label_names.include?(Labels::WORKFLOW_IN_DEV_LABEL)
    end

    def workflow_ready_for_review_added?
      event.added_label_names.include?(Labels::WORKFLOW_READY_FOR_REVIEW_LABEL)
    end

    def format_current_labels_list
      return 'none' unless event.label_names.any?

      label_names.map { |label| "`#{label}`" }.join(", ")
    end

    def label_names
      event.label_names
    end
  end
end
