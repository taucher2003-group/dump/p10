# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/constants/labels'

module Triage
  class RequireTypeOnRefinement < Processor
    READY_FOR_DEV_LABEL = 'workflow::ready for development'
    REFINEMENT_LABEL = 'workflow::refinement'

    react_to 'issue.update'

    def applicable?
      event.from_part_of_product_project? &&
        ready_for_development_added? &&
        !event.type_label_set? &&
        unique_comment.no_previous_comment?
    end

    def comment_cleanup_applicable?
      unique_comment.previous_comment? && event.type_label_set?
    end

    def process
      add_type_ignore
    end

    def processor_comment_cleanup
      unique_comment.delete_previous_comment
    end

    def documentation
      <<~TEXT
        This processor tags the actor in an issue when it progresses to workflow::ready for development but doesn't have a type::.
        Setting a type:: label is part of the refinement steps for engineering.
        https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/994#note_940356092
      TEXT
    end

    private

    def ready_for_development_added?
      event.added_label_names.include?(READY_FOR_DEV_LABEL)
    end

    def add_type_ignore
      comment = <<~MARKDOWN.chomp
        @#{event.event_actor_username}, thank you for moving this issue to ~"#{READY_FOR_DEV_LABEL}".

        Please add a `type::` label to indicate the
        [work type classification](https://handbook.gitlab.com/handbook/product/groups/product-analysis/engineering/metrics/#work-type-classification).
      MARKDOWN
      add_comment(unique_comment.wrap(comment), append_source_link: true)
    end
  end
end
