# frozen_string_literal: true

require_relative '../../triage/processor'
require 'retriable'

module Triage
  class VerifyBotApprovals < Processor
    react_to_approvals

    def applicable?
      event.from_gitlab_org? && event.gitlab_bot_event_actor?
    end

    def documentation
      <<~TEXT
        This processor verify bot approvals on merge requests.
        Only changes that had a formal review process on the security mirror
        can be approved automatically by the bot.
      TEXT
    end

    def process
      if allowed_usage?
        merge_or_retry
      else
        comment = <<~MARKDOWN.chomp
        :warning: This bot has been utilized to approve merge_requests outside
        of the allowed policies :warning:

        /unapprove
        MARKDOWN

        add_discussion(comment, append_source_link: true)
      end
    end

    private

    def allowed_usage?
      event.from_security_fork? &&
        event.source_branch_is?('master') &&
        event.target_branch_is_main_or_master?
    end

    def merge_or_retry
      # Sometimes our reactive approach is faster than the sidekiq jobs processing
      # the merge request. When this happen, the API request fail.
      # Here we make sure we retry up to two times in case of failure
      Retriable.retriable(base_interval: 5) do
        merge_merge_request
      end
    end
  end
end
