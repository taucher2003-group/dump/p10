# frozen_string_literal: true

require_relative '../../../../lib/constants/labels'
require_relative '../../../triage/event'
require_relative '../../../triage/processor'
require_relative '../../../job/pipeline_incident_sla/group_reminder_job'
require_relative '../../../job/pipeline_incident_sla/group_warning_job'
require_relative '../../../job/pipeline_incident_sla/stage_warning_job'
require_relative '../../../job/pipeline_incident_sla/dev_escalation_job'

module Triage
  module PipelineHealth
    class PipelineIncidentSlaEnforcer < Processor
      react_to 'incident.update'

      def applicable?
        event.from_master_broken_incidents_project? &&
          event.gitlab_bot_event_actor? &&
          event.added_label_names.include?(Labels::MASTER_BROKEN_INCIDENT_ESCALATION_LABELS[:needed])
      end

      def process
        Triage::PipelineIncidentSla::GroupReminderJob.perform_in(Triage::PipelineIncidentSla::GroupReminderJob::DELAY, event)
        Triage::PipelineIncidentSla::GroupWarningJob.perform_in(Triage::PipelineIncidentSla::GroupWarningJob::DELAY, event)
        Triage::PipelineIncidentSla::StageWarningJob.perform_in(Triage::PipelineIncidentSla::StageWarningJob::DELAY, event)
        Triage::PipelineIncidentSla::DevEscalationJob.perform_in(Triage::PipelineIncidentSla::DevEscalationJob::DELAY, event)
      end

      def documentation
        <<~TEXT
          This processor reminds group member to triage pipeline incidents within SLA and escalate in 3 hours of inactivity.
        TEXT
      end
    end
  end
end
