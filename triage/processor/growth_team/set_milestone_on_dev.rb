# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'
require_relative '../../../lib/constants/milestones'

module Triage
  class SetMilestoneOnDev < Processor
    react_to 'issue.update', 'issue.open'

    IN_DEV_LABEL = Labels::WORKFLOW_IN_DEV_LABEL
    GROWTH_LABELS = Labels::GROWTH_TEAM_LABELS
    GITLAB_ORG_GROUP_ID = 9970

    def applicable?
      event.from_gitlab_org? &&
        has_growth_labels? &&
        in_dev_label_added?
    end

    def process
      add_comment("/milestone %#{current_milestone_title}", append_source_link: false)
    end

    def documentation
      <<~TEXT
        This processor assigns the current milestone to an growth team issue
        when it is moved to `workflow::in dev`
      TEXT
    end

    private

    def has_growth_labels?
      GROWTH_LABELS.all? { |label| event.added_label_names.include?(label) || event.label_names.include?(label) }
    end

    def in_dev_label_added?
      event.added_label_names.include?(IN_DEV_LABEL)
    end

    def current_milestone_title
      @current_milestone_title ||= Triage.api_client.group_milestones(GITLAB_ORG_GROUP_ID, state: 'active').auto_paginate do |milestone|
        break milestone.title if milestone.title.match?(Milestones::VERSIONED_MILESTONE_PATTERN) && !milestone.expired
      end
    end
  end
end
