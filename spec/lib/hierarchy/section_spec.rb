# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/hierarchy/section'

RSpec.describe Hierarchy::Section do
  describe '.raw_data' do
    it 'returns WwwGitLabCom.sections data' do
      expect(described_class.raw_data).to include('section1' => hash_including('name' => 'Section 1'))
    end
  end
end
