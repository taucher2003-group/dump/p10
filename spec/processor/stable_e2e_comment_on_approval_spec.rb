# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/stable_e2e_comment_on_approval'
require_relative '../../triage/triage/event'

RSpec.describe Triage::StableE2eCommentOnApproval do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:approver_username) { 'approver' }
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:merge_request_iid) { 300 }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org_gitlab?: true,
        team_member_author?: true,
        maybe_automation_author?: false,
        jihu_contributor?: false,
        source_branch_is?: false,
        target_branch_is_stable_branch?: true,
        event_actor_username: approver_username,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  subject { described_class.new(event) }

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  let(:merge_request_notes) do
    [
      { body: "review comment 1" },
      { body: "review comment 2" }
    ]
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes?per_page=100",
      response_body: merge_request_notes)

    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  include_examples 'registers listeners', ['merge_request.approval', 'merge_request.approved']

  describe '#applicable?' do
    context 'when event is not from gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request target branch is not a stable branch' do
      before do
        allow(event).to receive(:target_branch_is_stable_branch?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only qa changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only templates changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => ".gitlab/issue_templates/Deprecations.md",
              "new_path" => ".gitlab/issue_templates/Deprecations.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only qa, docs, and templates changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            },
            {
              "old_path" => ".gitlab/issue_templates/Deprecations.md",
              "new_path" => ".gitlab/issue_templates/Deprecations.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains a mixture of docs and non-docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      include_examples 'event is applicable'
    end

    context 'when merge request contains a mixture of qa and non-qa changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            }
          ]
        }
      end

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'triggering a new pipeline' do
      it 'posts a discussion to inform that a new pipeline will be triggered and schedules a new pipeline 1 minute later' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request has been approved. Please ensure the `e2e:test-on-omnibus-ee` job
          has succeeded. If there is a failure, a Software Engineer in Test (SET) needs to confirm the failures are unrelated to the merge request.
          Ask for assistance on the `#s_developer_experience` Slack channel.
        MARKDOWN

        expect_discussion_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when merge request author is a gitlab-org member' do
      it_behaves_like 'triggering a new pipeline'
    end

    context 'when merge request author is automation bot' do
      before do
        allow(event).to receive(:team_member_author?).and_return(false)
        allow(event).to receive(:maybe_automation_author?).and_return(true)
      end

      it_behaves_like 'triggering a new pipeline'
    end
  end
end
