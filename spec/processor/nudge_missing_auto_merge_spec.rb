# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/nudge_missing_auto_merge'
require_relative '../../triage/triage/event'

RSpec.describe Triage::NudgeForMissingAutoMerge do
  include_context 'with event', Triage::PipelineEvent do
    let(:project_id)                       { Triage::Event::GITLAB_PROJECT_ID }
    let(:merge_request_iid)                { 300 }
    let(:pipeline_name)                    { 'Ruby 3.2 MR [tier:3, types:qa,code]' }
    let(:merged_results_pipeline)          { true }
    let(:latest_pipeline_in_merge_request) { true }
    let(:merge_when_pipeline_succeeds)     { true }
    let(:no_previous_comment?)             { true }
    let(:detailed_merge_status)            { 'mergeable' }

    let(:event_attrs) do
      {
        object_kind: 'pipeline',
        action: 'success',
        name: pipeline_name,
        merged_results_pipeline?: merged_results_pipeline,
        latest_pipeline_in_merge_request?: latest_pipeline_in_merge_request
      }
    end

    let(:payload) do
      {
        'project' => {
          'id' => project_id
        },
        'merge_request' => {
          'detailed_merge_status' => detailed_merge_status,
          'iid' => merge_request_iid,
          'target_project_id' => project_id
        }
      }
    end

    # Technical detail - When we repopulate the MergeRequest from the API, we do not use
    # the fields we previously got from the webhook: we take only fields from the API.
    #
    # This is the reason why we add the same fields here as in the webhook payload.
    let(:merge_request_from_api) do
      payload['merge_request'].merge(
        'merge_when_pipeline_succeeds' => merge_when_pipeline_succeeds
      )
    end
  end

  before do
    # Request to get the merge request from the API
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}",
      response_body: merge_request_from_api)

    allow(instance).to receive_message_chain(:unique_comment, :no_previous_comment?).and_return(no_previous_comment?)
  end

  let(:instance) { described_class.new(event) }

  subject { instance }

  include_examples 'registers listeners', ['pipeline.success']
  include_examples 'applicable on contextual event'

  RSpec.shared_examples 'writes a comment' do
    it 'writes a comment' do
      expect(instance).to receive(:add_comment).with(
        'our stubbed message',
        "/projects/#{project_id}/merge_requests/#{merge_request_iid}",
        append_source_link: true
      )

      subject.process
    end
  end

  RSpec.shared_examples 'does not write a comment' do
    it 'does not write a comment' do
      expect_no_request { subject.process }
    end
  end

  describe '#applicable?' do
    context 'when event project is gitlab-org/gitlab' do
      let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }

      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when event project is from another project' do
      let(:project_id) { Triage::Event::TRIAGE_OPS_PLAYGROUND_PROJECT_ID }

      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when pipeline is not a merged results pipeline' do
      let(:merged_results_pipeline) { false }

      include_examples 'event is not applicable'
    end

    context 'when pipeline is not the latest pipeline in the merge request' do
      let(:latest_pipeline_in_merge_request) { false }

      include_examples 'event is not applicable'
    end

    context 'when pipeline does not have a name' do
      let(:pipeline_name) { nil }

      include_examples 'event is not applicable'
    end

    context 'when pipeline is not a tier-3 pipeline' do
      let(:pipeline_name) { 'another pipeline name' }

      include_examples 'event is not applicable'
    end

    context 'when a comment was already added by the processor' do
      let(:no_previous_comment?) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:approvers) { [] }
    let(:body)      { nil }

    before do
      allow(event).to receive_message_chain(:merge_request_approvals, :approved_by).and_return(approvers)
    end

    context 'when we cannot find a last approver' do
      let(:approvers) { [] }

      include_examples 'does not write a comment'
    end

    context 'when we found a last approver' do
      let(:approvers) do
        [double('approver', user: double('user', username: 'last_approver'))] # rubocop:disable RSpec/VerifiedDoubles -- internal API
      end

      before do
        allow(instance).to receive_message_chain(:unique_comment, :wrap).with(body).and_return('our stubbed message')
      end

      context 'when auto-merge is set' do
        let(:merge_when_pipeline_succeeds) { true }

        context 'when detailed_merge_status is set to "mergeable"' do
          let(:detailed_merge_status) { 'mergeable' }

          include_examples 'does not write a comment'
        end

        context 'when detailed_merge_status is not set to "mergeable"' do
          context 'when the merge request status does not need to be resolved manually' do
            let(:detailed_merge_status) { 'approvals_syncing' }

            include_examples 'does not write a comment'
          end

          context 'when the merge request status needs to be resolved manually' do
            let(:detailed_merge_status) { 'discussions_not_resolved' }
            let(:body) do
              <<~MARKDOWN.chomp
                Hey there :wave:, could you please make sure this merge request gets merged?

                The merge request is set to auto-merge, but it is not currently mergeable (MR [`detailed_merge_status`](https://docs.gitlab.com/ee/api/merge_requests.html#merge-status) is **#{detailed_merge_status}**).
              MARKDOWN
            end

            include_examples 'writes a comment'
          end
        end
      end

      describe 'unique comment on the associated merge request' do
        let(:fake_unique_comment)   { instance_double(Triage::UniqueComment, wrap: stub_message) }
        let(:stub_message)          { 'our stubbed message' }
        let(:detailed_merge_status) { described_class::MR_DETAILED_STATUS_REQUIRING_MANUAL_ACTION.first }

        before do
          allow(instance).to receive(:unique_comment).and_call_original
          allow(instance).to receive(:add_comment).with(
            stub_message,
            "/projects/#{project_id}/merge_requests/#{merge_request_iid}",
            append_source_link: true
          )
        end

        it 'writes the comment on the associated merge request' do
          expect(Triage::UniqueComment).to receive(:new).with(
            described_class.to_s,
            noteable_object_kind: 'merge_request',
            noteable_resource_iid: merge_request_iid,
            noteable_project_id: project_id
          ).and_return(fake_unique_comment)

          subject.process
        end
      end
    end
  end
end
