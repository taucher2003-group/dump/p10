# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/apply_labels_from_related_issue'
require_relative '../../triage/triage/event'

RSpec.describe Triage::ApplyLabelsFromRelatedIssue do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:type_label_set) { false }
    let(:event_attrs) do
      {
        from_gitlab_org?: true,
        type_label_set?: type_label_set,
        description: description,
        project_id: 12
      }
    end

    let(:description) do
      <<~TEXT
      Lorem ipsum

      Related to #42.
      TEXT
    end

    let(:type_label_from_related_issue) { "type::feature" }
    let(:section_label_from_related_issue) { "section::section1" }
    let(:stage_label_from_related_issue) { "devops::stage with two groups" }
    let(:group_label_from_related_issue) { "group::group1" }
    let(:related_issue_labels) do
      [
        'foo',
        type_label_from_related_issue,
        section_label_from_related_issue,
        stage_label_from_related_issue,
        group_label_from_related_issue
      ]
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when description does not include a related issue' do
      let(:description) { 'Hello world' }

      include_examples 'event is not applicable'
    end

    context 'when description includes a related issue with a valid "type::*" label' do
      before do
        allow(subject).to receive(:related_issue_labels).and_return(['type::feature'])
      end

      include_examples 'event is applicable'

      context 'when the MR already has a "type::*" label' do
        let(:type_label_set) { true }

        include_examples 'event is not applicable'
      end
    end

    context 'when description includes a related issue with a invalid "*type::*" label' do
      before do
        allow(subject).to receive(:related_issue_labels).and_return(['prototype::feature'])
      end

      include_examples 'event is not applicable'
    end

    context 'when related issue has no labels to set' do
      before do
        allow(subject).to receive(:related_issue_labels).and_return(['foo'])
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    before do
      allow(subject).to receive(:related_issue_labels).and_return(related_issue_labels)
    end

    it 'posts a comment to add the detected labels' do
      body = <<~MARKDOWN.chomp
        /label ~"#{type_label_from_related_issue}" ~"#{section_label_from_related_issue}" ~"#{stage_label_from_related_issue}" ~"#{group_label_from_related_issue}"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    context 'with only a few related issue labels' do
      let(:related_issue_labels) { ['foo', type_label_from_related_issue] }

      it 'posts a comment to add the detected labels' do
        body = <<~MARKDOWN.chomp
          /label ~"#{type_label_from_related_issue}"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
