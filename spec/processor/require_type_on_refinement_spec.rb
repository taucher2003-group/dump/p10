# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/require_type_on_refinement'
require_relative '../../triage/triage/event'

RSpec.describe Triage::RequireTypeOnRefinement do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        from_part_of_product_project?: true,
        type_label_set?: type_label_set,
        added_label_names: added_label_names
      }
    end

    let(:type_label_set) { false }
    let(:added_label_names) { ['workflow::ready for development'] }
  end

  let(:notes) { [] }
  let(:previous_comment) do
    {
      'id' => 42,
      'body' => '<!-- triage-serverless RequireTypeOnRefinement -->'
    }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      verb: :get,
      path: "/projects/#{project_id}/issues/#{iid}/notes",
      query: { per_page: 100 },
      response_body: notes
    )
  end

  include_examples 'registers listeners', ['issue.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not a part of product' do
      before do
        allow(event).to receive(:from_part_of_product_project?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when ~"workflow::ready for development" is not added' do
      before do
        allow(event).to receive(:added_label_names).and_return([])
      end

      include_examples 'event is not applicable'
    end

    context 'when type label is set' do
      let(:type_label_set) { true }

      include_examples 'event is not applicable'
    end

    context 'when there is a previous comment' do
      let(:notes) { [previous_comment] }

      include_examples 'event is not applicable'
    end
  end

  describe '#comment_cleanup_applicable?' do
    context 'when there is no previous comment' do
      it_behaves_like 'event does not require process cleanup'
    end

    context 'when there is a previous comment' do
      let(:notes) { [previous_comment] }

      context 'when type label is not set' do
        it_behaves_like 'event does not require process cleanup'
      end

      context 'when type label is set' do
        let(:type_label_set) { true }

        it_behaves_like 'event requires process cleanup'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          <!-- triage-serverless RequireTypeOnRefinement -->
          @root, thank you for moving this issue to ~"workflow::ready for development".

          Please add a `type::` label to indicate the
          [work type classification](https://handbook.gitlab.com/handbook/product/groups/product-analysis/engineering/metrics/#work-type-classification).
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end

  describe '#processor_comment_cleanup' do
    let(:notes) { [previous_comment] }

    it 'deletes previous comment' do
      expect_api_request(
        verb: :delete,
        path: "/projects/#{project_id}/issues/#{iid}/notes/#{previous_comment['id']}",
        response_body: {}
      ) do
        subject.processor_comment_cleanup
      end
    end
  end
end
