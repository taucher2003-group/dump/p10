# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/track_organization_contributions'

RSpec.describe Triage::TrackOrganizationContributions do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:payload) do
      {
        'project' => {
          'name' => 'Name P'
        },
        'object_attributes' => {
          'iid' => 80,
          'url' => url,
          'username' => resource_author_username
        }
      }
    end

    let(:url) { 'http://test.com/name-p/-/merge_requests/15' }
  end

  let(:resource_author_username) { 'joe' }
  let(:detector) { instance_double(Triage::ContributingOrganizationFinder) }
  let(:org1) { { 'CONTRIBUTOR_ORGANIZATION' => 'Org 1', 'CONTRIBUTOR_USERNAMES' => %w[username_1 joe], 'Issue link' => 'http://link_1.com/issues/34' } }
  let(:wider_community_contribution?) { true }

  before do
    allow(Triage::ContributingOrganizationFinder).to receive(:new).and_return(detector)
    allow(detector).to receive(:find_by_user).with(resource_author_username).and_return(org1)
    allow(track_org_contributions).to receive(:wider_community_contribution?).and_return(wider_community_contribution?)
  end

  subject(:track_org_contributions) { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update', 'merge_request.merge']

  describe '#applicable?' do
    let(:no_previous_comment?) { true }

    before do
      allow(track_org_contributions).to receive_message_chain(:unique_comment, :no_previous_comment?).and_return(no_previous_comment?)
    end

    context 'when MR author is not from a contributing organization' do
      before do
        allow(detector).to receive(:find_by_user).with(resource_author_username).and_return(nil)
      end

      include_examples 'event is not applicable'

      context 'when assignee is from a contributing organization' do
        include_context 'with event', Triage::MergeRequestEvent do
          let(:payload) do
            {
              'project' => {
                'name' => 'Name P'
              },
              'object_attributes' => {
                'iid' => 80,
                'url' => url,
                'username' => resource_author_username
              },
              'assignees' => [
                {
                  'username' => 'username_2'
                }
              ]
            }
          end

          let(:url) { 'http://test.com/name-p/-/merge_requests/16' }
        end

        let(:org2) { { 'CONTRIBUTOR_ORGANIZATION' => 'Org 2', 'CONTRIBUTOR_USERNAMES' => %w[username_2], 'Issue link' => 'http://link_2.com/issues/45' } }

        before do
          allow(detector).to receive(:find_by_user).with('username_2').and_return(org2)
        end

        include_examples 'event is applicable'
      end
    end

    context 'when MR author is from a contributing organization' do
      include_examples 'event is applicable'
    end

    context 'when MR is not wider community contribution' do
      let(:wider_community_contribution?) { false }

      include_examples 'event is not applicable'
    end

    context 'when unique MR comment is already posted in the issue' do
      let(:no_previous_comment?) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:org1_data) do
      {
        issue_iid: 34,
        name: org1['CONTRIBUTOR_ORGANIZATION']
      }
    end

    it 'posts a comment' do
      track_org_contributions.instance_variable_set(:@organizations, [org1_data])
      comment = add_automation_suffix do
        <<~MARKDOWN.chomp
          <!-- triage-serverless TrackOrganizationContributions - Project ID #{event.project_id} - MR ID #{event.iid} -->
          This MR #{url}+ has been identified as being contributed by Org 1
        MARKDOWN
      end

      expect_comment_request(event: event, body: comment, noteable_path: '/projects/62760330/issues/34') do
        track_org_contributions.process
      end
    end
  end
end
