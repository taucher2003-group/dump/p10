# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/quick_win_first_time_contributor_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::QuickWinFirstTimeContributorLabel do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'update',
        weight: weight,
        added_label_names: labels_added,
        description: issue_description,
        from_gitlab_group?: from_gitlab_group
      }
    end

    let(:from_gitlab_group) { true }
    let(:labels_added) { ['quick win::first-time contributor'] }
    let(:weight) { 1 }

    let(:issue_description) do
      <<~DESCRIPTION
          ## Implementation plan
          This is the implementation plan
      DESCRIPTION
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.open", "issue.reopen", "issue.update"]

  describe '#applicable?' do
    where(:from_gitlab_group, :labels_added, :weight, :issue_description, :expected) do
      [
        # Behaves as expected (from gitlab org/com/community/components)
        [true, ['quick win::first-time contributor'], nil, '', 'event is applicable'],

        # Event is not from gitlab org, com, community, or components
        [false, ['quick win::first-time contributor'], nil, '', 'event is not applicable'],

        # Event is not adding the quick win::first-time contributor label
        [true, ['frontend'], nil, '', 'event is not applicable'],

        # Implementation plan provided, we check weight
        [true, ['quick win::first-time contributor'], 5, "## Implementation plan\nPlan description", 'event is applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation plan\nPlan description", 'event is not applicable'],

        # Weight is correct, we check the implementation plan
        [true, ['quick win::first-time contributor'], 1, 'short description', 'event is applicable'],
        [true, ['quick win::first-time contributor'], 1, "# Implementation plan\n", 'event is applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation plan\n", 'event is applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation plan\n\n\n", 'event is applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation plan\n# First level heading\nImplementation plan on the wrong line", 'event is applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation plan\n## Second level heading\nImplementation plan on the wrong line", 'event is applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation plan\n### Third level heading", 'event is applicable'],
        [true, ['quick win::first-time contributor'], 1, "### Implementation plan\n", 'event is applicable'],

        # Third level heading with content or extra headers
        [true, ['quick win::first-time contributor'], 1, "## Implementation plan\n### Third level heading\nThis is the implementation plan", 'event is not applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation plan\n### Third level heading\nThis is the implementation plan\n## Unrelated heading", 'event is not applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation abc\nThis is the implementation plan", 'event is not applicable'],
        [true, ['quick win::first-time contributor'], 1, "## Implementation\nThis is the implementation plan", 'event is not applicable']
      ]
    end

    with_them do
      it 'behaves as expected' do
        expect(subject).not_to be_applicable if expected == 'event is not applicable'
        expect(subject).to be_applicable if expected == 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#post_quick_win_first_time_contributor_label_guide_comment' do
    let(:unique_message_identifier) { '<!-- triage-serverless QuickWinFirstTimeContributorLabel -->' }

    context 'when there is no previous comment' do
      before do
        allow_any_instance_of(Triage::UniqueComment).to receive(:no_previous_comment?).and_return(true)
        allow_any_instance_of(Triage::UniqueComment).to receive(:previous_comment?).and_return(false)
        allow_any_instance_of(Triage::UniqueComment).to receive(:resource_notes).and_return([])
      end

      it 'posts full guide comment with labeling commands' do
        expected_body = add_automation_suffix do
          <<~MARKDOWN.chomp
            #{unique_message_identifier}
            @root thanks for adding the ~"#{Labels::QUICK_WIN_FIRST_TIME_CONTRIBUTOR_LABEL}" label!

             However, it has been automatically removed because this issue does not meet all of the required criteria for the label to be applied:
               - Must be assigned a weight between 0-1
               - Must include an implementation plan as a second or third level heading
               - Should include at least 1 GitLab team member or experienced community contributor (e.g., “Support contact: @username”) tagged in the Implementation plan section

             You can refer to the [criteria for ~"quick win::first-time contributor" issues documentation](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows/#criteria-for-quick-winfirst-time-contributor-issues) for an up-to-date list of the requirements.

            /unlabel ~"#{Labels::QUICK_WIN_FIRST_TIME_CONTRIBUTOR_LABEL}"
            /label ~"#{Labels::AUTOMATION_QUICK_WIN_REMOVED}"
          MARKDOWN
        end

        expect_comment_request(event: event, body: expected_body.strip) do
          subject.send(:process)
        end
      end
    end

    context 'when there is a previous comment' do
      before do
        allow_any_instance_of(Triage::UniqueComment).to receive(:no_previous_comment?).and_return(false)
        allow_any_instance_of(Triage::UniqueComment).to receive(:previous_comment?).and_return(true)
        allow_any_instance_of(Triage::UniqueComment).to receive(:resource_notes).and_return([])
      end

      it 'posts only labeling commands' do
        expected_body = <<~COMMANDS.strip
        /unlabel ~"#{Labels::QUICK_WIN_FIRST_TIME_CONTRIBUTOR_LABEL}"
        /label ~"#{Labels::AUTOMATION_QUICK_WIN_REMOVED}"
        COMMANDS

        expect_comment_request(event: event, body: expected_body) do
          subject.send(:process)
        end
      end
    end

    describe '#labeling_commands' do
      it 'returns the correct labeling commands' do
        expected_commands = <<~COMMANDS.strip
          /unlabel ~"#{Labels::QUICK_WIN_FIRST_TIME_CONTRIBUTOR_LABEL}"
          /label ~"#{Labels::AUTOMATION_QUICK_WIN_REMOVED}"
        COMMANDS

        expect(subject.send(:labeling_commands).strip).to eq(expected_commands)
      end
    end
  end
end
