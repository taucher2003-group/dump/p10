# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/resources/issue'
require_relative '../../../triage/resources/milestone'

RSpec.describe Triage::Issue do
  let(:issue_attr) do
    { 'iid' => '222',
      'project_id' => '123',
      'milestone' => milestone_attr,
      'labels' => ['some labels'] }
  end

  let(:milestone_attr_base) do
    { 'id' => '456',
      'title' => 'group milestone' }
  end

  let(:milestone_attr) { nil }

  subject { described_class.new(issue_attr) }

  describe '#milestone' do
    context 'when issue is assigned to a group milestone' do
      let(:group_id)       { '678' }
      let(:milestone_attr) { milestone_attr_base.merge({ 'group_id' => group_id }) }
      let(:path)           { "/groups/#{group_id}/milestones/#{milestone_attr['id']}" }

      it 'makes request to the group milestone endpoint and returns a Triage::Milestone' do
        expect_api_request(path: path, response_body: milestone_attr) do
          expect(subject.milestone).to be_a(Triage::Milestone)
          expect(subject.milestone.attributes).to eq(milestone_attr)
        end
      end
    end

    context 'when issue is assigned to a project milestone' do
      let(:project_id)     { '678' }
      let(:milestone_attr) { milestone_attr_base.merge({ 'project_id' => project_id }) }
      let(:path)           { "/projects/#{project_id}/milestones/#{milestone_attr['id']}" }

      it 'makes requests to the project milestone endpoint and returns a Triage::Milestone' do
        expect_api_request(path: path, response_body: milestone_attr) do
          expect(subject.milestone).to be_a(Triage::Milestone)
          expect(subject.milestone.attributes).to eq(milestone_attr)
        end
      end
    end

    context 'when milestone is nil' do
      it 'returns nil' do
        expect(subject.milestone).to be_nil
      end
    end
  end

  describe '#labels' do
    it 'returns labels from issue attributes' do
      expect(subject.labels).to eq(issue_attr['labels'])
    end
  end
end
