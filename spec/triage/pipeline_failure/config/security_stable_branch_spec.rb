# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/security_stable_branch'

RSpec.describe Triage::PipelineFailure::Config::SecurityStableBranch do
  let(:project_path_with_namespace) { 'gitlab-org/security/gitlab' }
  let(:ref) { '15-10-stable-ee' }
  let(:source) { 'push' }
  let(:source_job_id) { nil }
  let(:event_actor) { Triage::User.new(id: 42) }

  let(:merge_request) { Triage::MergeRequest.build_from_api(author: { 'id' => 42 }) }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      project_path_with_namespace: project_path_with_namespace,
      source: source,
      ref: ref,
      event_actor: event_actor,
      merge_request: merge_request,
      source_job_id: source_job_id)
  end

  before do
    allow(event).to receive(:on_instance?).with(:com).and_return(true)
  end

  subject { described_class.new(event) }

  describe '.match?' do
    subject { described_class.match?(event) }

    it { is_expected.to be_truthy }

    context 'when event is not on the .com instance' do
      before do
        allow(event).to receive(:on_instance?).with(:com).and_return(false)
      end

      it { is_expected.to be_falsy }
    end

    context 'when project_path_with_namespace is not "gitlab-org/security/gitlab"' do
      let(:project_path_with_namespace) { 'foo/bar' }

      it { is_expected.to be_falsy }
    end

    context 'when ref is not /^[\d-]+-stable(-ee)?$/' do
      let(:ref) { 'foo' }

      it { is_expected.to be_falsy }
    end

    context 'when source_job_id is present' do
      let(:source_job_id) { '42' }

      it { is_expected.to be_falsy }
    end
  end

  describe '#incident_project_id' do
    it 'returns expected projet id' do
      expect(subject.incident_project_id).to eq('5064907')
    end
  end

  describe '#incident_template' do
    it 'returns expected template' do
      expect(subject.incident_template).to eq(described_class::INCIDENT_TEMPLATE)
    end
  end

  describe '#incident_labels' do
    it 'returns expected labels' do
      expect(subject.incident_labels).to eq(%w[release-blocker stable-branch-failure])
    end
  end

  describe '#incident_extra_attrs' do
    it 'returns expected attrs' do
      expect(subject.incident_extra_attrs).to eq({ assignee_ids: [event_actor.id, merge_request.author['id']] })
    end
  end

  describe '#default_slack_channels' do
    it 'returns expected channels' do
      expect(subject.default_slack_channels).to eq(['releases'])
    end
  end
end
