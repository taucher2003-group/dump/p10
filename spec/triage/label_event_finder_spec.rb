# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/label_event_finder'

RSpec.describe Triage::LabelEventFinder do
  let(:project_id)    { 111 }
  let(:resource_iid)  { 222 }
  let(:resource_type) { 'issue' }
  let(:label_name)    { 'severity::1' }
  let(:action)        { 'remove' }

  let(:context) do
    {
      project_id: project_id,
      resource_iid: resource_iid,
      resource_type: resource_type,
      label_name: label_name,
      action: action
    }
  end

  let(:date) { "2020-01-19T22:25:45.529Z" }
  let(:path) { "/projects/#{project_id}/issues/#{resource_iid}/resource_label_events" }
  let(:label_events) do
    [{
      created_at: date,
      resource_type: "Issue",
      resource_id: 459,
      label: {
        name: label_name
      },
      action: "remove"
    }]
  end

  subject { described_class.new(context) }

  describe '#label_added_date' do
    context 'when matching issue label event is found' do
      it 'returns the created_at date for that event' do
        expect_api_request(path: path, response_body: label_events) do
          expect(subject.label_added_date).to eq(date)
        end
      end
    end

    context 'when matching merge request label event is found' do
      let(:resource_type) { 'merge_reqeust' }
      let(:path)          { "/projects/#{project_id}/merge_requests/#{resource_iid}/resource_label_events" }

      it 'returns the created_at date for that event' do
        expect_api_request(path: path, response_body: label_events) do
          expect(subject.label_added_date).to eq(date)
        end
      end
    end

    context 'when no matching event is found' do
      let(:action) { 'add' }

      it 'returns nil' do
        expect_api_request(path: path, response_body: label_events) do
          expect(subject.label_added_date).to be_nil
        end
      end
    end
  end

  describe '#label_added_username' do
    # rubocop:disable RSpec/VerifiedDoubles
    let(:user) { double('User', username: 'test_user') }
    let(:label_event) { double('LabelEvent', user: user) }
    # rubocop:enable RSpec/VerifiedDoubles
    let(:finder) { described_class.new(project_id: 1, resource_iid: 2, resource_type: 'issue', label_name: 'test', action: 'add') }

    before do
      allow(finder).to receive(:label_event).and_return(label_event)
    end

    it 'returns the username of the user who added the label' do
      expect(finder.label_added_username).to eq('test_user')
    end

    context 'when label_event is nil' do
      before do
        allow(finder).to receive(:label_event).and_return(nil)
      end

      it 'returns nil' do
        expect(finder.label_added_username).to be_nil
      end
    end
  end
end
