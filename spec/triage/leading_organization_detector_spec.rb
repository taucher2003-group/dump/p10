# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/leading_organization_detector'

RSpec.describe Triage::LeadingOrganizationDetector, :clean_cache do
  let(:username) { 'leader' }
  let(:regular_username) { 'regular' }

  describe '#leading_organization?' do
    let(:credential_path) { '' }
    let(:sheet_id) { '' }

    before do
      allow(ENV).to receive(:fetch).with('LEADING_ORGS_TRACKER_SHEET_ID', '').and_return(sheet_id)

      allow(ENV).to receive(:fetch).with('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH', '').and_return(credential_path)
      stub_googleapis_auth_request(credential_path)
      stub_read_leading_organization_users_request
    end

    context 'with GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env var not defined' do
      it 'warns the variable is not defined and returns nil' do
        expect(subject).to receive_message_chain(:logger, :warn).with('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env variable not defined.')

        expect(subject.leading_organization?(username)).to be_nil
      end
    end

    context 'with LEADING_ORGS_TRACKER_SHEET_ID env var not defined' do
      let(:credential_path) { 'path/to/credentials' }

      it 'warns the variable is not defined and returns nil' do
        expect(subject).to receive_message_chain(:logger, :warn).with('LEADING_ORGS_TRACKER_SHEET_ID env variable not defined.')

        expect(subject.leading_organization?(username)).to be_nil
      end
    end

    context 'with GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env var defined' do
      let(:credential_path) { 'path/to/credentials' }
      let(:sheet_id) { 'SHEET_ID' }

      context 'with valid user id' do
        context 'when present in the CSV' do
          it 'returns true' do
            expect(subject.leading_organization?(username)).to be(true)
          end
        end

        context 'when present in the CSV in different case' do
          let(:username) { 'LeadeR' }

          it 'returns true' do
            expect(subject.leading_organization?(username)).to be(true)
          end
        end

        context 'when not present in the CSV' do
          let(:username) { regular_username }

          it 'returns false' do
            expect(subject.leading_organization?(username)).to be(false)
          end
        end
      end

      context 'when username is nil' do
        let(:username) { nil }

        it 'returns false' do
          expect(subject.leading_organization?(username)).to be(false)
        end
      end
    end
  end
end
