# frozen_string_literal: true

# rubocop:disable RSpec/InstanceVariable
# This context expects:
# - artifacts: a { artifact_pah => artifact_content } hash
# - project_id
# - job_id
RSpec.shared_context 'with stubbed GitLab artifact' do
  let(:api_client_double) { instance_double(Gitlab::Client) }

  before do
    allow(Triage).to receive(:api_client).and_return(api_client_double)

    @artifact_files ||= []

    artifacts.each do |artifact_path, artifact_content|
      artifact_file = Gitlab::FileResponse.new(Tempfile.new(File.basename(artifact_path)))
      @artifact_files << artifact_file

      allow(api_client_double)
        .to receive(:download_job_artifact_file)
        .with(project_id, job_id, artifact_path)
        .and_return(artifact_file)

      artifact_file.write(artifact_content)
      artifact_file.rewind
    end
  end

  after do
    @artifact_files.map(&:unlink)
  end
end
# rubocop:enable RSpec/InstanceVariable
