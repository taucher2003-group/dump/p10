# frozen_string_literal: true

RSpec.shared_examples 'processor documentation is present' do
  it 'contains documentation' do
    expect(subject.documentation).to be_present
  end
end
